# frozen_string_literal: true

module Gcs
  class SbomConverter
    SCHEMA_VERSION = "1"

    PROPERTY_NAME_SCHEMA_VERSION = 'gitlab:meta:schema_version'
    PROPERTY_NAME_IMAGE_NAME = 'gitlab:container_scanning:image:name'
    PROPERTY_NAME_IMAGE_TAG = 'gitlab:container_scanning:image:tag'
    PROPERTY_NAME_OPERATING_SYSTEM = 'gitlab:container_scanning:operating_system'
    PROPERTY_NAME_OPERATING_SYSTEM_NAME = "#{PROPERTY_NAME_OPERATING_SYSTEM}:name".freeze
    PROPERTY_NAME_OPERATING_SYSTEM_VERSION = "#{PROPERTY_NAME_OPERATING_SYSTEM}:version".freeze

    TRIVY_OS_TO_TRIVYDB_BUCKET_MAPPING = {
      "cbl-mariner" => "CBL-Mariner",
      "oracle" => "Oracle Linux",
      "photon" => "Photon OS",
      "redhat" => "Red Hat",
      "suse linux enterprise server" => "SUSE Linux Enterprise",
      "opensuse.leap" => "openSUSE Leap",
      "amazon" => "amazon linux"
    }.freeze

    def initialize(source)
      @source = source
    end

    def convert
      @report = JSON.parse(source)

      report["metadata"]["properties"] ||= []

      image = report["metadata"]["component"]

      push_property(PROPERTY_NAME_SCHEMA_VERSION, SCHEMA_VERSION)
      push_property(PROPERTY_NAME_IMAGE_NAME, image_name(image))
      push_property(PROPERTY_NAME_IMAGE_TAG, image_tag(image))

      push_property(PROPERTY_NAME_OPERATING_SYSTEM_NAME, operating_system_name)
      push_property(PROPERTY_NAME_OPERATING_SYSTEM_VERSION, operating_system_version)

      fix_report_version!

      report.to_json
    end

    private

    attr_reader :source, :report

    def image_name(image)
      image["name"].split(":")[0]
    end

    def image_tag(image)
      image["name"].split(":")[1]
    end

    def operating_system
      @operating_system ||= report["components"]&.find { |component| component["type"] == "operating-system" }
    end

    def operating_system_name
      return unless operating_system

      trivy_os_to_trivydb_bucket(operating_system["name"])
    end

    def operating_system_version
      return unless operating_system

      operating_system["version"]
    end

    def trivy_os_to_trivydb_bucket(name)
      TRIVY_OS_TO_TRIVYDB_BUCKET_MAPPING.fetch(name.downcase, name)
    end

    def push_property(name, value)
      report["metadata"]["properties"].push({ name:, value: }) if name && value
    end

    # force cyclonedx sbom version to one supported by the rails monolith
    # https://gitlab.com/gitlab-org/gitlab/-/issues/431406
    def fix_report_version!
      report['specVersion'] = '1.4'
      report['$schema'] = 'http://cyclonedx.org/schema/bom-1.4.schema.json'
    end
  end
end
